# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'bauk/core/version'

Gem::Specification.new do |spec|
  spec.name          = 'bauk-core'
  spec.version       = Bauk::Core::VERSION
  spec.authors       = ['Kuba Jasko']
  spec.email         = ['kubajasko@hotmail.co.uk']

  spec.summary       = 'Core functionality/modules used by bauk gems'
  spec.description   = 'This gem includes things such as log and parser' \
                       ' wrappers, used by other bauk gems'
  spec.homepage      = 'https://gitlab.com/BAUK/bauk-core'
  spec.license       = 'MIT'

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/BAUK/bauk-core'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/BAUK/bauk-core/commits/master'

  # Specify which files should be added to the gem when it is released.
  # This loads the files in the RubyGem that have been added to git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      f.match(%r{^(test|spec|features)/})
    end
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.74'
end
