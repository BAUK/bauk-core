# frozen_string_literal: true

require 'bauk/core/version'

module Bauk
  module Core
    class Error < StandardError; end
    # Your code goes here...
  end
end
