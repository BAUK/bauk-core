# frozen_string_literal: true

class Array
  def join_custom(map = {})
    map[:delim] ||= ', '
    map[:end_delim] ||= ' and '
    case length
    when 0
      ''
    when 1
      self[0].to_s.dup
    else
      "#{self[0...-1].join(map[:delim])}#{map[:end_delim]}#{self[-1]}"
    end
  end
end
