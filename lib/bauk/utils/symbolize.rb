# frozen_string_literal: true

class Object
  def symbolize
    self
  end
end
class Hash
  def symbolize
    reduce({}) do |memo, (k, v)|
      memo.tap do |m|
        m[k.to_sym] = v.respond_to?(:symbolize) ? v.symbolize() : v
      end
    end
  end
end
class Array
  def symbolize
    each_with_object([]) do |v, memo|
      memo.push v.respond_to?(:symbolize) ? v.symbolize() : v
    end
  end
end
