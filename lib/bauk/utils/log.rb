# frozen_string_literal: true

module Bauk
  module Utils
    # This mixin ia a wrapper around log4r and enables logging functionality.
    # It auto-names loggers in relation to the class they are included in,
    #  unless a name is specified.
    module Log
      require 'log4r'
      Log4r.define_levels(*Log4r::Log4rConfig::LogLevels)

      # Creates a new Log4r instance with the provided name
      # Unless one already exists, in which case it returns it
      # It also adds it to the list of loggers for future verboseness changing
      def self.create(name)
        log = loggers[name][:instance]
        if log.nil?
          log = Log4r::Logger.new(logger_name name)
          log.level = Log.loggers[name][:level]
          Log.loggers[name][:outputters].each { |o| log.add o }
          log.debug("Initialized loger #{name} to log level: #{log.level}")
          loggers[name] = loggers[name].clone # Take default values
          loggers[name][:instance] = log
        end
        log
      end

      # Function to turn a name into a more valid logger name
      # e.g. The::Best::Class -> TB.class
      def self.logger_name(name)
        logger_parts = name.split '::'
        logger_name = logger_parts.pop
        unless logger_parts.empty?
          logger_name = [logger_parts.map {|p| p[0]}.join(''), logger_name].join('.')
        end
        logger_name
      end

      # Increases logging from all loggers, unless a name is specified
      # (See set_log_level for runes on name provided).
      #  Due to the inverse of log4r, more logging means a lower log level
      def self.increase_logging(name = '')
        set_log_level(-1, name, true)
      end

      # Decreases logging from all loggers, unless a name is specified
      # (See set_log_level for runes on name provided).
      #  Due to the inverse of log4r, less logging means a higher log level
      def self.decrease_logging(name = '')
        set_log_level(1, name, true)
      end

      # Sets the log level for all loggers matching the name.
      # If the add flag is set, it increases/decreases the log level by the specified amount.
      # The name is as follows:
      # '':: Blank to edit all loggers
      # logger_name:: To edit that logger and any child loggers
      def self.set_log_level(level, name = '', add = false)
        level += loggers[name][:level] if add
        level = check_level(level)
        if name != ''
          # In case not set yet, set the logger to default values
          # (If name = false, we want to edit the default directly)
          loggers[name] = loggers[name].clone
        end
        loggers[name][:level] = level
        # Change all active Child loggers
        loggers.each do |n, logger|
          next unless n =~ /^#{name}(::.*)*$/ || name == ''

          logger[:level] = level
          logger[:instance].level = level if logger[:instance]
        end
      end

      # Returns the log_level of the provided logger
      # If no name provided, returns the default log level
      def self.log_level(name = '')
        loggers[name][:level]
      end

      private_class_method
      # Stores the list of created loggers
      @@loggers_list = nil
      def self.loggers
        @@loggers_list ||= Hash.new(level: Log4r::WARN,
                                    outputters: [Log4r::Outputter.stderr])
        @@loggers_list
      end

      # FATAL = 5, DEBUG = 1, so high level is less logging...
      def self.check_level(level)
        if level > Log4r::FATAL
          puts "Log level #{level} too high, setting to max(#{Log4r::FATAL})"
          level = Log4r::FATAL
        elsif level < Log4r::DEBUG
          puts "Log level #{level} too low, setting to min (#{Log4r::DEBUG})"
          level = Log4r::DEBUG
        end
        level
      end

      # INSTANCE METHODS::

      # Used to access log4r inside a class which includes this class
      def log
        if @log.nil?
          logger_name = self.class.name
          logger_name = "#{self.name}@" if self.respond_to? "name"
          @log = Log.create(logger_name || 'anonymous') if @log.nil?
        end
        @log
      end

      # Prints the designated title to the screen with an underline
      def title(string)
        space = 4
        overhang = 1
        puts ' ' * space + string
        puts((' ' * space + string.gsub(/./, '=')).sub(' ' * overhang + '=', '=' * overhang + '=').sub(/$/, '=' * overhang))
      end

      # Increases logging globally for the name of this logger.
      # See ::increase_logging
      def increase_logging
        Log.increase_logging(log.name)
      end

      # Decreases logging globally for the name of this logger.
      # See ::decrease_logging
      def decrease_logging
        Log.decrease_logging(log.name)
      end

      # Setts the log level of this logger only
      def log_level=(level)
        log.level = Log.check_level(level)
      end

      # Prints a variety of log messages at each log level.
      def test_logging
        log.debug 'DEBUG'
        log.info 'INFO'
        log.warn 'WARN'
        log.error 'ERROR'
        log.fatal 'FATAL'
      end
    end
  end
end
