# frozen_string_literal: true

module Bauk
  module Core
    VERSION = `git describe --tags --always`.sub(/^v/, '')
  end
end
