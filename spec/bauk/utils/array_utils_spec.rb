# frozen_string_literal: true

require 'bauk/utils/array_utils'

RSpec.describe 'Array.join_custom' do
  it 'returns empty string with empty array' do
    expect([].join_custom).to eql ''
    expect([].join_custom(delim: ' 2 ')).to eql ''
    expect([].join_custom(end_delim: ' 111 ')).to eql ''
  end
  it 'Returns just the item with just one item' do
    expect([1].join_custom).to eql '1'
    expect([1].join_custom(delim: ' 2 ')).to eql '1'
    expect([1].join_custom(end_delim: ' 111 ')).to eql '1'
  end
  it 'Returns a simple join with just two items' do
    expect([1, 5].join_custom).to eql '1 and 5'
    expect([1, 5].join_custom(delim: ' or ')).to eql '1 and 5'
    expect([1, 5].join_custom(end_delim: ' or ')).to eql '1 or 5'
  end
  it 'Joins long lists with commas and an final "and"' do
    list = [1, 3, 5, 7, 9, 3, 6, 9]
    expect(list.join_custom).to eql '1, 3, 5, 7, 9, 3, 6 and 9'
    expect(list.join_custom(delim: ' or ')).to eql '1 or 3 or 5 or 7 or 9 or 3 or 6 and 9'
    expect(list.join_custom(end_delim: ' or ')).to eql '1, 3, 5, 7, 9, 3, 6 or 9'
  end
end
