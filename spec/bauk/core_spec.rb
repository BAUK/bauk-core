# frozen_string_literal: true

RSpec.describe Bauk::Core do
  it 'has a version number' do
    expect(Bauk::Core::VERSION).not_to be nil
  end
end
